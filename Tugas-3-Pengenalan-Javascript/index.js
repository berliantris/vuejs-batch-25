// soal 1
var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"
var gabung = pertama.substring(0,4) + " " + pertama.substring(12,18) + " " + kedua.substring(0,18)
console.log(gabung)


// soal 2
var kataPertama = "10"
var kataKedua = "2"
var kataKetiga = "4"
var kataKeempat = "6"

var int1 = Number(kataPertama)
var int2 = Number(kataKedua)
var int3 = Number(kataKetiga)
var int4 = Number(kataKeempat)

var hasil = (int2*int3)+(int1+int4)
console.log(hasil)


// soal 3
var kalimat = 'wah javascript itu keren sekali'

var kataPertama = kalimat.substring(0, 3)
var kataKedua = kalimat.substring(4, 14)
var kataKetiga = kalimat.substring(15, 18)
var kataKeempat = kalimat.substring(19, 24)
var kataKelima = kalimat.substring(25, 31)

console.log('Kata Pertama: ' + kataPertama)
console.log('Kata Kedua: ' + kataKedua)
console.log('Kata Ketiga: ' + kataKetiga)
console.log('Kata Keempat: ' + kataKeempat)
console.log('Kata Kelima: ' + kataKelima)